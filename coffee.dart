import 'dart:io';

class Drink {
  var _name;
  var _price;
  var _sweetness;
  bool straw = false;
  bool lid = false;

  getName() {
    return _name;
  }

  getPrice() {
    return _price;
  }

  setName(name) {
    _name = name;
  }

  setPrice(price) {
    _price = price;
  }

  setSweet(sweet) {
    _sweetness = sweet;
  }

  setStraw(b) {
    if (b) {
      straw = true;
    }
  }

  setLid(b) {
    if (b) {
      lid = true;
    }
  }

  Drink(name, price) {
    _name = name;
    _price = price;
  }

  text(n) {
    return "${n}.${_name} ${_price} บาท";
  }

  summary() {
    return "\n${_name} ${_sweetness} ${_price} บาท";
  }
}

class DrinkType {
  var _name;
  List<Drink> _drinkList = [];

  getName() {
    return _name;
  }

  getDrinks() {
    return _drinkList;
  }

  setName(name) {
    _name = name;
  }

  setDrinks(drinks) {
    _drinkList = drinks;
  }

  DrinkType(String name, {drinks: 0}) {
    _name = name;
    if (drinks != 0) {
      _drinkList = drinks;
    }
  }

  addDrink(name, price) {
    _drinkList.add(new Drink(name, price));
  }

  showDrinks() {
    print("\n${_name}");
    for (int i = 0; i < _drinkList.length; i++) {
      print(_drinkList[i].text(i + 1));
    }
  }

  getSelectedDrink(index) {
    return _drinkList[index];
  }
}

void main() {
  DrinkType recommended = new DrinkType("เมนูแนะนำ");
  DrinkType coffee = new DrinkType("กาแฟ");
  DrinkType tea = new DrinkType("ชา");
  DrinkType milk = new DrinkType("นม โกโก้และคาราเมล");
  DrinkType protein = new DrinkType("โปรตีนเช็ค");
  DrinkType etc = new DrinkType("น้ำโซดาและอื่นๆ");

  recommended.addDrink('เอสเพรสโซ่', 25); // Espresso
  recommended.addDrink('โกโก้เย็น', 35); //  Cold Cocoa
  recommended.addDrink('นมบราวน์ชูก้าเย็น', 40); //  Iced Brown Sugar Milk
  recommended.addDrink('ชาไทยเย็น', 40); // Iced Thai Tea
  recommended.addDrink('น้ำมะนาวโซดา', 20); // lemon juice soda

  coffee.addDrink('เอสเพรสโซ่', 25); // Espresso
  coffee.addDrink('ลาเต้ร้อน', 35); // Hot latte
  coffee.addDrink('คาปูชิโนร้อน', 35); // Hot cappuccino
  coffee.addDrink('มอคค่าร้อน', 40); // Hot mocha
  coffee.addDrink('กาแฟดำร้อน', 20); // Hot black coffee

  tea.addDrink('เก๊กฮวยร้อน', 20); // Hot chrysanthemum
  tea.addDrink('ชาไทยเย็น', 35); // Iced Thai Tea
  tea.addDrink('ชานมไต้หวัน', 35); //  Taiwan Milk Tea
  tea.addDrink('มัทฉะลาเต้ร้อน', 45); //  Hot Matcha Latte
  tea.addDrink('ชามะนาวร้อน', 20); // Hot lemon tea

  milk.addDrink('นมคาราเมลร้อน', 35); // Hot caramel milk
  milk.addDrink('นมร้อน', 30); // Hot milk
  milk.addDrink('โกโก้เย็น', 35); // Cold Cocoa
  milk.addDrink('นมเย็น', 30); // Iced milk
  milk.addDrink('นมชมพูเย็น', 40); // Iced Pink milk

  protein.addDrink('มัทฉะโปรตีน', 60); // Matcha Protein
  protein.addDrink('โกโก้โปรตีน', 60); // Cocoa Protein
  protein.addDrink('ชาไต้หวันโปรตีน', 60); // Taiwan tea Protein
  protein.addDrink('คาราเมลโปรตีน', 60); // Caramel Protein
  protein.addDrink('นมโปรตีน', 60); // Milk Protein

  etc.addDrink('เป๊ปซี่น้ำแข็ง', 15); // pepsi
  etc.addDrink('น้ำมะนาวโซดา', 20); // lemon juice soda
  etc.addDrink('น้ำแดงโซดา', 20); // red soda
  etc.addDrink('โซดา', 10); // soda
  etc.addDrink('น้ำดื่ม', 10); // water

  List<DrinkType> menu = [recommended, coffee, tea, milk, protein, etc];
  var sweet = ['ไม่ใส่น้ำตาล', 'หวานน้อย', 'หวานพอดี', 'หวานมาก', 'หวาน 3 โลก'];
  var curr_drink;
  var pay_method;

  print('ยินดีต้อนรับสู่ร้านเต่าบิน\nกรุณาเลือกประเภทเครื่องดื่ม');
  for (int i = 0; i < menu.length; i++) {
    print('${i + 1}.${menu[i].getName()}');
  }

  print("\nประเภทเครื่องดื่ม : ");
  int? selected_type = int.parse(stdin.readLineSync()!) - 1;
  menu[selected_type].showDrinks();

  print("\nเมนู : ");
  int? selected_drink = int.parse(stdin.readLineSync()!) - 1;
  curr_drink = menu[selected_type].getSelectedDrink(selected_drink);

  print("\nเลือกระดับความหวาน");
  int i = 1;
  sweet.forEach((s) => print('${i++}.${s}'));

  print("\nความหวาน : ");
  int? selected_sweet = int.parse(stdin.readLineSync()!) - 1;
  curr_drink.setSweet(sweet[selected_sweet]);

  print("\nรับหลอดไหม : yes/no ");
  String? straw_in = stdin.readLineSync()!;
  curr_drink.setStraw(straw_in == 'yes');

  print("รับฝาไหม : yes/no ");
  String? lid_in = stdin.readLineSync()!;
  curr_drink.setLid(lid_in == 'yes');

  print(curr_drink.summary());

  print("\nเลือกวิธีการชำระเงิน : เงินสด/สแกน QR");
  print("\nชำระเงิน : ");
  String? payment = stdin.readLineSync()!;
  pay_method = payment;
  print("\nรอเสิร์ฟ 30 วินาที");
}

// ยินดีต้อนรับสู่ร้านเต่าบิน
// กรุณาเลือกประเภทเครื่องดื่ม
// 1.เมนูแนะนำ
// 2.กาแฟ
// 3.ชา
// 4.นม โกโก้และคาราเมล
// 5.โปรตีนเช็ค
// 6.น้ำโซดาและอื่นๆ

// ประเภทเครื่องดื่ม :
// 1

// เมนูแนะนำ
// 1.เอสเพรสโซ่ 25 บาท
// 2.โกโก้เย็น 35 บาท
// 3.นมบราวน์ชูก้าเย็น 40 บาท
// 4.ชาไทยเย็น 40 บาท
// 5.น้ำมะนาวโซดา 20 บาท

// เมนู :
// 1

// เลือกระดับความหวาน
// 1.ไม่ใส่น้ำตาล
// 2.หวานน้อย
// 3.หวานพอดี
// 4.หวานมาก
// 5.หวาน 3 โลก

// ความหวาน :
// 1

// รับหลอดไหม : yes/no
// No
// รับฝาไหม : yes/no 
// No

// เอสเพรสโซ่ ไม่ใส่น้ำตาล 25 บาท

// เลือกวิธีการชำระเงิน : เงินสด/สแกน QR

// ชำระเงิน :
// เงินสด

// รอเสิร์ฟ 30 วินาที